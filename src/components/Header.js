import React from "react"
import { View, Text, Platform } from "react-native"
const Header = props => {
  return (
    <View
      style={{
        alignItems: "center",
        justifyContent: "center",
        shadowColor: "black",
        height: Platform.OS === "ios" ? 0 : 80
      }}
    >
      <Text style={{ fontSize: 20 }}>{props.title}</Text>
    </View>
  )
}

export default Header
